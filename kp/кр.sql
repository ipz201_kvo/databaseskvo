--������� �.�. ���-20-1 ������ 9
create database Hospital
use Hospital
--��������� �� �� �������
create table Pacient(
id_pacient int IDENTITY(1,1) not null primary key,
name_pacient nvarchar(50),
date_of_birth date,
gender nvarchar(50),
social_status nvarchar(50)
)


create table Treatment(
id_treatment int IDENTITY(1,1) not null primary key,
diagnos nvarchar(50),
date_of_starting date default getdate(),
status_pacient nvarchar(50),
id_pacient int not null references Pacient(id_pacient) on update cascade on delete cascade,
id_doctor int not null references Doctor(id_doctor) on update cascade on delete cascade,
)

create table Doctor (
id_doctor int IDENTITY(1,1) not null primary key,
name_doctor nvarchar(50),
spec nvarchar(50),
kvalification nvarchar(50),
)

--������� ����� � �������

select * from Pacient
select * from Treatment
select * from Doctor



insert into Pacient values ('������ ����� ��������','2002-01-17','�','���������')
insert into Pacient values ('�������� ���� ���㳿���','1954-11-08','�','��������')
insert into Pacient values ('����� ���� �������������','1940-03-01','�','��������')
insert into Pacient values ('������ ���� ���������','1963-01-03','�','��������')
insert into Pacient values ('������� ���� ����������','1972-03-25','�','������')
--insert into Pacient values ('','','','')



insert into Treatment values('���','2022-09-04','�������', 1, 2)
insert into Treatment values('�ϲ�','2020-06-01','�����', 2, 1)
insert into Treatment values('����','2020-11-07','����������', 3, 1)
insert into Treatment values('��������� ������','2021-03-08','���������� � ���������', 4, 2)
insert into Treatment values('�����','2020-03-05','�����', 5, 3)
insert into Treatment values('ʳ�','2021-11-09','�������', 1, 3)
--insert into Treatment values('','','', , )



insert into Doctor values ('������� ������� ��������','����������','1')
insert into Doctor values ('������ ������ ���������','��������','2')
insert into Doctor values ('������ ����� ��������','������������','3')



--2 ��������. ������ 1 ���������� �� ����� �� ��� SQL.
--��������� � �������, ���� ������������ ������� ������ ��������� ������-��������������
select Treatment.diagnos, Doctor.spec
from Treatment inner join Doctor on Treatment.id_doctor=Doctor.id_doctor
where Treatment.diagnos='������� ������' and Doctor.spec= '������������'

--3.	������ 2 ���������� �� �������������  
--������� ����� ��� ������, ������� �������� ������������� � ������������. 
go
create view task3 as
select Pacient.id_pacient, Pacient.name_pacient, Pacient.social_status, Doctor.id_doctor, Doctor.name_doctor
from  Treatment inner join Pacient on Treatment.id_pacient=Pacient.id_pacient
inner join Doctor on Treatment.id_doctor=Doctor.id_doctor
where  Pacient.social_status='��������' 

--4.	������ 3 � ���������� �� ���������, �� ���������� ��� ���������
--���������� ������� ���������� �� ����������� '������'. 

go
create proc  task4 as
declare @dead float, @all float

set @dead=(
select count(id_treatment)
from Treatment
where  Treatment.diagnos='�����' and Treatment.status_pacient='�����')

set @all=(select count(Treatment.id_treatment) as '�-��� �����������' from Treatment)

select ((@dead*100)/@all)+'%' as '�-��� ��������'


exec task4



--5.	������ 4 ���������� �� ��������� � �������� �� ��������� ����������� 
--���������, ������� ������/������ ����� ���������. 
go
create proc task5 @d nvarchar(50), @out int out as
set @out=(select Pacient.id_pacient from Pacient)
select Pacient.name_pacient from Treatment
join Pacient on Treatment.id_pacient=Pacient.id_pacient
where Treatment.diagnos LIKE @d



declare @o int
exec task5 N'�����', @o output
print @o



--6.	���������� ������ �� ��������� ���������
-- ����������� ���� ������� ��������
go--����������� - ����_�����������
create trigger task6 on Treatment for insert as

if(select datediff(year, (select top(1) inserted.date_of_starting from inserted), getdate()))>0

begin
	raiserror (N'�������! ���� ����������� �������� ������!!!', 16, 1)
	rollback transaction
end



insert into Treatment values('������� ������','2030-12-01','�������', 1, 2)
