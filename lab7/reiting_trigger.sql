--1.
CREATE TRIGGER Student_Insert ON dbo_student INSTEAD OF INSERT AS
IF NOT EXISTS(
	SELECT * FROM inserted I
	JOIN dbo_groups G on I.Kod_group = G.Kod_group)
BEGIN
	INSERT INTO dbo_groups(Kod_group, K_navch_plan)
	SELECT Kod_group, 13
	FROM inserted
	INSERT INTO dbo_student(Sname, Name, Fname, N_ingroup, Kod_group)
	SELECT Sname, [Name], Fname, N_ingroup, Kod_group
	FROM inserted
END
ELSE
BEGIN
	INSERT INTO dbo_student(Sname, Name, Fname, N_ingroup, Kod_group)
	SELECT Sname, Name, Fname, N_ingroup, Kod_group
	FROM inserted
END
select * from dbo_student
select * from dbo_groups
INSERT INTO dbo_student (Sname, Name, Fname, N_ingroup, Kod_group) VALUES ('�������', '�����', '�������������', '1', 'ϲ-56')
--2.
drop TRIGGER Students_Update
CREATE TRIGGER Students_Update ON dbo_student AFTER DELETE AS
IF EXISTS(
	SELECT * FROM dbo_groups G
	LEFT JOIN dbo_student S ON S.Kod_group = G.Kod_group
	WHERE kilk IS NULL)
BEGIN
DELETE FROM dbo_groups
WHERE Kod_group IN
(SELECT G.Kod_group FROM dbo_groups G
	LEFT JOIN dbo_student S ON S.Kod_group = G.Kod_group
	WHERE kilk IS NULL)
END  
delete from dbo_student where Kod_group = 'ϲ-56'
