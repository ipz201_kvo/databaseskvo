use torg_firm
--������ � ��� ��������� ����������� ������
--1
GO
create view qresult as 
select zakaz_tovar.id_zakaz, klient.Nazva, zakaz_tovar.id_tovar,
zakaz_tovar.Kilkist, zakaz_tovar.Znigka, tovar.Price*zakaz_tovar.Kilkist*(1-zakaz_tovar.Znigka) as zag_vartist
from (klient inner join zakaz on klient.id_klient = zakaz.id_klient)
inner join (tovar inner join zakaz_tovar on tovar.id_tovar = zakaz_tovar.id_tovar)
on zakaz.id_zakaz = zakaz_tovar.id_zakaz
GO
--2
create view qresult2 as
select qresult.id_zakaz, klient.Nazva, zakaz.date_naznach,
sum(qresult.zag_vartist) as Itog
from (klient inner join zakaz on klient.id_klient = zakaz.id_klient) inner join
qresult on zakaz.id_zakaz = qresult.id_zakaz
group by qresult.id_zakaz, klient.Nazva, zakaz.date_naznach
GO
--3
select sotrudnik.Fname, sotrudnik.Name, sotrudnik.Posada,
avg(qresult2.Itog) as [avg-itog]
from (sotrudnik inner join zakaz on sotrudnik.id_sotrud=zakaz.id_sotrud)
inner join qresult2 on zakaz.id_zakaz = qresult2.id_zakaz
group by sotrudnik.Fname, sotrudnik.Name, sotrudnik.Posada
--4
SELECT TOP 3 id_zakaz, sum(zakaz_tovar.Kilkist*tovar.Price) as summa FROM zakaz_tovar, tovar
GROUP BY id_zakaz
ORDER BY sum(zakaz_tovar.Kilkist) desc
--5
SELECT klient.Nazva, Sum(QResult.Zag_vartist) AS [Sum-Zag_vartist]
FROM (klient INNER JOIN zakaz ON klient.id_klient = zakaz.id_klient) INNER
JOIN QResult ON zakaz.id_zakaz = QResult.id_zakaz
GROUP BY klient.Nazva;
--6
SELECT Tovar.Nazva, Max((Tovar.Price*zakaz_tovar.Kilkist*(1-
Zakaz_tovar.Znigka))) AS Zag_vartist
FROM Tovar INNER JOIN zakaz_tovar ON Tovar.id_tovar=zakaz_tovar.id_tovar
GROUP BY Tovar.Nazva;
--7
SELECT sotrudnik.Fname, sotrudnik.Name, sotrudnik.Posada,
Count(zakaz.id_zakaz) AS [Count-K_zakaz]
FROM zakaz INNER JOIN sotrudnik ON zakaz.id_sotrud = sotrudnik.id_sotrud
GROUP BY sotrudnik.Fname, sotrudnik.Name, sotrudnik.Posada;
--1
SELECT Klient.Nazva, Postachalnik.Nazva
FROM Klient, Postachalnik
WHERE Klient.City=Postachalnik.City;
--2
SELECT Tovar.Nazva
FROM Tovar
WHERE (Tovar.Price>(SELECT AVG (Price)
FROM Tovar));
--3
SELECT *
FROM Tovar
WHERE id_tovar IN (SELECT id_tovar FROM Zakaz_tovar 
WHERE Znigka >=0.05);
--4
INSERT INTO Tovar (Nazva, Price, id_postav )
VALUES ('�����', CAST(12.50 AS Decimal(6, 2)), 1);
--5
INSERT INTO Klient ( nazva )
SELECT Nazva
FROM Postachalnik
WHERE Postachalnik.city='�������';
--6
UPDATE Tovar SET Price = (1.13*Price)
WHERE id_postav=1;
--7
DELETE FROM Tovar
WHERE id_tovar=25;
--8
select *from zakaz
DELETE FROM Zakaz
WHERE id_klient = (SELECT id_klient FROM Klient WHERE Nazva = '��� "����"');
--9
SELECT Postachalnik.Nazva, Postachalnik.City, Postachalnik.Adress
FROM Postachalnik
UNION 
SELECT Klient.Nazva, Klient.City, Klient.Adress
FROM Klient;
--10
Declare @min decimal(10,4) = 0.5, @date_begin DateTime = '01.07.2017'
SELECT zakaz_tovar.id_zakaz, zakaz.date_rozm, tovar.price, zakaz_tovar.kilkist
FROM zakaz INNER JOIN (Tovar INNER JOIN zakaz_tovar 
ON Tovar.id_tovar=zakaz_tovar.id_tovar) 
ON zakaz.id_zakaz=zakaz_tovar.id_zakaz
WHERE tovar.price>@min And zakaz.date_rozm>=@date_begin;
--������ ��� ����������� ���������
--1,2,3
select count(Nazva), 'tovariv' from tovar
union
select count(Name), 'sotrudnik' from sotrudnik
union
select count(Nazva), 'postachalnik' from postachalnik
--4
select nazva, count(tovar.NaSklade) as [summa]
from (tovar inner join zakaz_tovar on tovar.id_tovar = zakaz_tovar.id_tovar)
join zakaz on zakaz.id_zakaz = zakaz_tovar.id_zakaz
where zakaz.date_naznach between '20.06.2017' And '20.07.2017'
group by tovar.Nazva
--5
select sum(summa) from
(select sum(tovar.Price) as summa from tovar left join zakaz_tovar on tovar.id_tovar = zakaz_tovar.id_tovar
right join zakaz on zakaz.id_zakaz = zakaz_tovar.id_zakaz
group by zakaz.date_naznach, tovar.Nazva, tovar.Price
having zakaz.date_naznach between '20.06.2017' And '20.07.2017') as summatovar
--6
select postachalnik.Nazva, sum(tovar.Price*zakaz_tovar.Kilkist) as [summa]
from (postachalnik join tovar on postachalnik.id_postach = tovar.id_postav)
join zakaz_tovar on tovar.id_tovar = zakaz_tovar.id_tovar
group by postachalnik.Nazva
--7
select postach.Nazva, count(zakaz_tovar.Kilkist) from zakaz_tovar inner join
(select postachalnik.Nazva, id_tovar from tovar 
left join postachalnik on postachalnik.id_postach=tovar.id_postav
where tovar.Nazva = '������' group by postachalnik.Nazva, id_tovar)
as postach on postach.id_tovar = zakaz_tovar.id_tovar 
where postach.id_tovar = zakaz_tovar.id_tovar
group by postach.Nazva
--8
select tovar.Nazva, avg(tovar.price*zakaz_tovar.Kilkist) as [average]
from (tovar join zakaz_tovar on zakaz_tovar.id_tovar = tovar.id_tovar)
group by tovar.Nazva
--9
select klient.Nazva, sum(zakaz_tovar.Kilkist*tovar.Price) as summa
from (zakaz_tovar join tovar on tovar.id_tovar = zakaz_tovar.id_tovar)
join (klient join zakaz on klient.id_klient= zakaz.id_klient)
on zakaz.id_zakaz = zakaz_tovar.id_zakaz
where klient.City = '�������'
group by klient.Nazva
--10
select tovar.Nazva as nazva, postachalnik.Nazva as postch_name, avg(tovar.Price) as average
from (tovar inner join postachalnik on postachalnik.id_postach = tovar.id_postav)
group by tovar.Nazva, postachalnik.Nazva