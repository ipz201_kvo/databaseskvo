use vetclinic

create table treatment(
id_treatment int IDENTITY(1,1) not null primary key,
name_treatment varchar(50),
price_uah money check (price_uah > 0),
duration_treatment varchar(50),
diagnosis nvarchar(50) constraint diagnosis_check check (diagnosis like '[�-�]%'),
allergic nvarchar(50) constraint allergic_check check (allergic like '[�-�]%')
);

create table building_clinic(
id_�linic int IDENTITY(1,1) not null primary key,
number_corpus int constraint corpus_format check (number_corpus like '[1-9]'),
address_build nvarchar(50),
number_ward int constraint ward_check check (number_ward like '[1-9]%'),
date_coming date);

create table doctor(
id_doctor int IDENTITY(1,1) not null primary key,
name_doctor varchar(50) constraint namedoctor_check check (name_doctor like '[�-�]%'),
surname_doctor varchar(50) constraint surnamedoctor_check check (surname_doctor like '[�-�]%'),
telephone_doctor varchar(50) constraint telephonedoctor_check check (telephone_doctor like '([0-9][0-9][0-9])[0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
specialization varchar(50) constraint specialization_check check (specialization like '[�-�]%'),
adress_doctor nvarchar(50),
experience_doctor date,
date_birth_doctor date,
sex_doctor char(1),
salary_doctor_uah money check (salary_doctor_uah > 0)
);

create table animal
(id_animal int IDENTITY(1,1) not null primary key,
name_animal varchar(50) constraint name_format check (name_animal like '[�-�]%'),
data_birth date,
animal_species varchar(50) constraint species_format check (animal_species like '[�-�]%'),
weight_kg int check (weight_kg > 0),
sex char(1) constraint sex_format check (sex like '�|�')
);
 
create table master_animal(
id_master int IDENTITY(1,1) not null primary key,
name_master varchar(20) constraint namemaster_check check (name_master like '[�-�]%'),
surname_master varchar(20) constraint surnamemaster_check check (surname_master like '[�-�]%'),
telephone_master varchar(12) constraint telephonemaster_check check (telephone_master like '([0-9][0-9][0-9])[0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
adress_master varchar(50),
email_master varchar(20) constraint emailmaster_check check (email_master like '.+@.+'),
discount_master char(1) constraint discount_check check (discount_master like '+|-')
);

ALTER TABLE [animal] add id_master int NOT NULL
ALTER TABLE [animal] add foreign key (id_master) references master_animal(id_master) on delete no action on update cascade 

ALTER TABLE [animal] add id_doctor int NOT NULL
ALTER TABLE [animal] add foreign key (id_doctor) references doctor(id_doctor) on delete no action on update cascade

ALTER TABLE [animal] add id_treatment int NOT NULL
ALTER TABLE [animal] add foreign key (id_treatment) references treatment(id_treatment) on delete no action on update cascade

ALTER TABLE [animal] add id_clinic int NOT NULL
ALTER TABLE [animal] add foreign key (id_clinic) references building_clinic(id_�linic) on delete no action on update cascade

insert into treatment Values('˳�������', '6000', '������������ �����', '�������', '³������')
insert into treatment Values('˳�������', '5500', '��� �����', '�������', '���')
insert into treatment Values('����������', '3000', '̳����', '��������', '�������')

insert into building_clinic Values('3', '���.��� ������� 1', '331', '2021-07-09')
insert into building_clinic Values('4', '���.������ �������� 21', '196', '2021-01-13')
insert into building_clinic Values('1', '���.������ ������������ 41', '120', '2021-06-01')

insert into doctor Values('�����', '³�������', '(063)1234445', '��������', '���.������������� 12 ��.34', '2019-12-23', '1977-05-03', '�', '32000')
insert into doctor Values('����', '����������', '(093)1231185', 'ճ����', '���.³������� 12 ��.102', '2015-06-11', '1978-03-23', '�', '10000')
insert into doctor Values('���������', '˳���', '(068)3833113', '����������', '���.����������� 83', '2021-01-14', '2001-11-01', '�', '14000')

insert into master_animal Values('���������', '������', '(096)9466378', '���.���������� 113 ��.1', 'vladislavsavchuk@gmail.com', '+')
insert into master_animal Values('������', '������', '(068)0065277', '���.������������ 4 ��.7', 'nikitatimkow@gmail.com', '+')
insert into master_animal Values('�����', '����', '(096)7748333', '���.ϳ���� 5','olgazmur@gmail.com', '-' )

select *from master_animal
select *from animal
select *from doctor
select *from treatment
select *from building_clinic

insert into animal Values('������','2015-01-13','������','8','�','2','5','2','1')
insert into animal Values('����','2020-11-14','ʳ��','520','�','6','6','1','3')
insert into animal Values('������','2021-07-05','ʳ�','2','�','3','4','3','2')

