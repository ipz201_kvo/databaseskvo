create database torg_firm
go
USE [torg_firm]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[klient](
	[id_klient] [int] IDENTITY(1,1) NOT NULL,
	[Nazva] [varchar](50) NULL,
	[Adress] [varchar](50) NULL,
	[City] [varchar](50) NULL,
	[Tel] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[id_klient] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[postachalnik](
	[id_postach] [int] NOT NULL,
	[Nazva] [varchar](50) NULL,
	[Adress] [varchar](50) NULL,
	[City] [varchar](50) NULL,
	[Tel] [varchar](50) NULL,
	[Kontakt_osoba] [varchar](50) NULL,
	[Posada] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[id_postach] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sotrudnik](
	[id_sotrud] [int] NOT NULL,
	[Fname] [varchar](50) NULL,
	[Name] [varchar](50) NULL,
	[Lname] [varchar](50) NULL,
	[Posada] [varchar](50) NULL,
	[Adress] [varchar](50) NULL,
	[City] [varchar](50) NULL,
	[Home_tel] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[id_sotrud] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tovar](
	[id_tovar] [int] IDENTITY(1,1) NOT NULL,
	[Nazva] [varchar](50) NULL,
	[Price] [decimal](6, 2) NULL,
	[NaSklade] [int] NULL,
	[id_postav] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id_tovar] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[zakaz](
	[id_zakaz] [int] IDENTITY(1,1) NOT NULL,
	[id_klient] [int] NOT NULL,
	[id_sotrud] [int] NOT NULL,
	[date_rozm] [datetime] NULL,
	[date_naznach]  AS (dateadd(day,(10),CONVERT([date],[date_rozm]))),
PRIMARY KEY CLUSTERED 
(
	[id_zakaz] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[zakaz_tovar](
	[id_zakaz] [int] NOT NULL,
	[id_tovar] [int] NOT NULL,
	[Kilkist] [int] NOT NULL,
	[Znigka] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id_zakaz] ASC,
	[id_tovar] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
INSERT [dbo].[klient] ([Nazva], [Adress], [City], [Tel]) VALUES (N'�� ����� �.�.', N'���. ������������ 10', N'�������', N'0504345566')
INSERT [dbo].[klient] ([Nazva], [Adress], [City], [Tel]) VALUES (N'��� "����"', N'���. ������� 7', N'�������', N'0678889994')
INSERT [dbo].[klient] ([Nazva], [Adress], [City], [Tel]) VALUES (N'�� ���� �.�.', N'���. ��������� 9', N'���', N'0501112233')
INSERT [dbo].[klient] ([Nazva], [Adress], [City], [Tel]) VALUES (N'�� ����� �.�.', N'���. ��������� 44', N'�����', N'0807933561')
INSERT [dbo].[klient] ([Nazva], [Adress], [City], [Tel]) VALUES (N'��� "����"', N'���. ����������� 76', N'�������', N'0340989204')
INSERT [dbo].[klient] ([Nazva], [Adress], [City], [Tel]) VALUES (N'�� ������ �.�.', N'���. �������� 4', N'����', N'0781296218')
select *from klient

INSERT [dbo].[postachalnik] ([id_postach], [Nazva], [Adress], [City], [Tel], [Kontakt_osoba], [Posada]) VALUES (1, N'�� ���� �.�.', N'���. ������������ 46', N'�������', N'0509998877', N'���� �.�.', N'')
INSERT [dbo].[postachalnik] ([id_postach], [Nazva], [Adress], [City], [Tel], [Kontakt_osoba], [Posada]) VALUES (2, N'��� "����"', N'���. ������� 7', N'�������', N'0678889994', N'������ �.�', N'��������')
INSERT [dbo].[postachalnik] ([id_postach], [Nazva], [Adress], [City], [Tel], [Kontakt_osoba], [Posada]) VALUES (3, N'���� �.�', N'���. ³����� 9', N'���', N'0974445544', N'���� �.�.', N'')
INSERT [dbo].[postachalnik] ([id_postach], [Nazva], [Adress], [City], [Tel], [Kontakt_osoba], [Posada]) VALUES (4, N'�� ����� �.�.', N'���. ����������� 1', N'�����', N'0674838817', N'����� �.�.', N'')
INSERT [dbo].[postachalnik] ([id_postach], [Nazva], [Adress], [City], [Tel], [Kontakt_osoba], [Posada]) VALUES (5, N'��� "����"', N'���. ������������������ 47', N'����', N'0675812944', N'������ �.�', N'���������')
INSERT [dbo].[postachalnik] ([id_postach], [Nazva], [Adress], [City], [Tel], [Kontakt_osoba], [Posada]) VALUES (6, N'������� �.�.', N'���. ��������� 86', N'���', N'0967238744', N'������� �.�.', N'')
select *from postachalnik

INSERT [dbo].[sotrudnik] ([id_sotrud], [Fname], [Name], [Lname], [Posada], [Adress], [City], [Home_tel]) VALUES (1, N'����', N'������', N'���㳿���', N'���������', N'���. ������������ 67 ��.20', N'�������', N'0509998877')
INSERT [dbo].[sotrudnik] ([id_sotrud], [Fname], [Name], [Lname], [Posada], [Adress], [City], [Home_tel]) VALUES (2, N'��������', N'������', N'�����������', N'���������', N'���. ³����� 20 ��.50', N'�������', N'0506667788')
INSERT [dbo].[sotrudnik] ([id_sotrud], [Fname], [Name], [Lname], [Posada], [Adress], [City], [Home_tel]) VALUES (3, N'��������', N'�����', N'�������', N'���������-�����������', N'���. ������� 67 ��.20', N'�������', N'0509998877')
INSERT [dbo].[sotrudnik] ([id_sotrud], [Fname], [Name], [Lname], [Posada], [Adress], [City], [Home_tel]) VALUES (4, N'������', N'�����', N'�������������', N'���������', N'���. ����������� 7 ��.88', N'�������', N'0509539807')
INSERT [dbo].[sotrudnik] ([id_sotrud], [Fname], [Name], [Lname], [Posada], [Adress], [City], [Home_tel]) VALUES (5, N'���������', N'������', N'�����������', N'���������', N'���. ��������� 29 ��.4', N'�������', N'0679067238')
INSERT [dbo].[sotrudnik] ([id_sotrud], [Fname], [Name], [Lname], [Posada], [Adress], [City], [Home_tel]) VALUES (6, N'������', N'�������', N'������������', N'���������-�����������', N'���. ������ 12 ��.95', N'�������', N'0983728037')
select *from sotrudnik

SET IDENTITY_INSERT [dbo].[tovar] ON 

INSERT [dbo].[tovar] ([id_tovar], [Nazva], [Price], [NaSklade], [id_postav]) VALUES (1, N'������', CAST(18.00 AS Decimal(6, 2)), 50, 1)
INSERT [dbo].[tovar] ([id_tovar], [Nazva], [Price], [NaSklade], [id_postav]) VALUES (2, N'������', CAST(16.00 AS Decimal(6, 2)), 50, 2)
INSERT [dbo].[tovar] ([id_tovar], [Nazva], [Price], [NaSklade], [id_postav]) VALUES (3, N'�����', CAST(21.00 AS Decimal(6, 2)), 25, 3)
INSERT [dbo].[tovar] ([id_tovar], [Nazva], [Price], [NaSklade], [id_postav]) VALUES (4, N'�������', CAST(10.00 AS Decimal(6, 2)), 30, 2)
INSERT [dbo].[tovar] ([id_tovar], [Nazva], [Price], [NaSklade], [id_postav]) VALUES (5, N'������', CAST(35.00 AS Decimal(6, 2)), 10, 1)
INSERT [dbo].[tovar] ([id_tovar], [Nazva], [Price], [NaSklade], [id_postav]) VALUES (6, N'������', CAST(18.00 AS Decimal(6, 2)), 50, 1)
INSERT [dbo].[tovar] ([id_tovar], [Nazva], [Price], [NaSklade], [id_postav]) VALUES (7, N'������', CAST(16.00 AS Decimal(6, 2)), 50, 2)
INSERT [dbo].[tovar] ([id_tovar], [Nazva], [Price], [NaSklade], [id_postav]) VALUES (8, N'�����', CAST(21.00 AS Decimal(6, 2)), 25, 3)
INSERT [dbo].[tovar] ([id_tovar], [Nazva], [Price], [NaSklade], [id_postav]) VALUES (9, N'�������', CAST(10.00 AS Decimal(6, 2)), 30, 2)
INSERT [dbo].[tovar] ([id_tovar], [Nazva], [Price], [NaSklade], [id_postav]) VALUES (10, N'������', CAST(35.00 AS Decimal(6, 2)), 10, 1)
select *from tovar
SET IDENTITY_INSERT [dbo].[tovar] OFF
SET IDENTITY_INSERT [dbo].[zakaz] ON 

INSERT [dbo].[zakaz] ([id_zakaz], [id_klient], [id_sotrud], [date_rozm]) VALUES (1, 4, 2, CAST(N'2017-07-10T07:11:51.293' AS DateTime))
INSERT [dbo].[zakaz] ([id_zakaz], [id_klient], [id_sotrud], [date_rozm]) VALUES (2, 3, 4, CAST(N'2017-07-07T07:11:51.293' AS DateTime))
INSERT [dbo].[zakaz] ([id_zakaz], [id_klient], [id_sotrud], [date_rozm]) VALUES (3, 1, 3, CAST(N'2017-06-22T07:11:51.293' AS DateTime))
INSERT [dbo].[zakaz] ([id_zakaz], [id_klient], [id_sotrud], [date_rozm]) VALUES (4, 2, 1, CAST(N'2017-07-06T07:11:51.293' AS DateTime))
INSERT [dbo].[zakaz] ([id_zakaz], [id_klient], [id_sotrud], [date_rozm]) VALUES (5, 4, 5, CAST(N'2017-06-30T07:11:51.297' AS DateTime))
INSERT [dbo].[zakaz] ([id_zakaz], [id_klient], [id_sotrud], [date_rozm]) VALUES (6, 6, 2, CAST(N'2017-06-21T07:11:51.297' AS DateTime))
INSERT [dbo].[zakaz] ([id_zakaz], [id_klient], [id_sotrud], [date_rozm]) VALUES (7, 5, 6, CAST(N'2017-06-22T07:11:51.297' AS DateTime))
INSERT [dbo].[zakaz] ([id_zakaz], [id_klient], [id_sotrud], [date_rozm]) VALUES (8, 2, 2, CAST(N'2017-07-11T07:11:51.297' AS DateTime))
INSERT [dbo].[zakaz] ([id_zakaz], [id_klient], [id_sotrud], [date_rozm]) VALUES (9, 6, 4, CAST(N'2017-06-16T07:11:51.297' AS DateTime))
INSERT [dbo].[zakaz] ([id_zakaz], [id_klient], [id_sotrud], [date_rozm]) VALUES (10, 5, 6, CAST(N'2017-06-28T07:11:51.297' AS DateTime))
select *from zakaz
SET IDENTITY_INSERT [dbo].[zakaz] OFF

INSERT [dbo].[zakaz_tovar] ([id_zakaz], [id_tovar], [Kilkist], [Znigka]) VALUES (1, 10, 32, 0)
INSERT [dbo].[zakaz_tovar] ([id_zakaz], [id_tovar], [Kilkist], [Znigka]) VALUES (2, 9, 5, 0)
INSERT [dbo].[zakaz_tovar] ([id_zakaz], [id_tovar], [Kilkist], [Znigka]) VALUES (3, 8, 4, 0)
INSERT [dbo].[zakaz_tovar] ([id_zakaz], [id_tovar], [Kilkist], [Znigka]) VALUES (4, 7, 40, 0)
INSERT [dbo].[zakaz_tovar] ([id_zakaz], [id_tovar], [Kilkist], [Znigka]) VALUES (5, 6, 7, 0)
INSERT [dbo].[zakaz_tovar] ([id_zakaz], [id_tovar], [Kilkist], [Znigka]) VALUES (6, 5, 17, 0)
INSERT [dbo].[zakaz_tovar] ([id_zakaz], [id_tovar], [Kilkist], [Znigka]) VALUES (7, 4, 16, 0)
INSERT [dbo].[zakaz_tovar] ([id_zakaz], [id_tovar], [Kilkist], [Znigka]) VALUES (8, 3, 9, 0)
INSERT [dbo].[zakaz_tovar] ([id_zakaz], [id_tovar], [Kilkist], [Znigka]) VALUES (9, 2, 16, 0)
INSERT [dbo].[zakaz_tovar] ([id_zakaz], [id_tovar], [Kilkist], [Znigka]) VALUES (10, 1, 21, 0)
INSERT [dbo].[zakaz_tovar] ([id_zakaz], [id_tovar], [Kilkist], [Znigka]) VALUES (8, 10, 2, 0)
INSERT [dbo].[zakaz_tovar] ([id_zakaz], [id_tovar], [Kilkist], [Znigka]) VALUES (7, 9, 13, 0)
INSERT [dbo].[zakaz_tovar] ([id_zakaz], [id_tovar], [Kilkist], [Znigka]) VALUES (6, 8, 2, 0)
INSERT [dbo].[zakaz_tovar] ([id_zakaz], [id_tovar], [Kilkist], [Znigka]) VALUES (9, 7, 5, 0)
INSERT [dbo].[zakaz_tovar] ([id_zakaz], [id_tovar], [Kilkist], [Znigka]) VALUES (10, 6, 11, 0)
INSERT [dbo].[zakaz_tovar] ([id_zakaz], [id_tovar], [Kilkist], [Znigka]) VALUES (5, 5, 41, 0)
INSERT [dbo].[zakaz_tovar] ([id_zakaz], [id_tovar], [Kilkist], [Znigka]) VALUES (4, 4, 35, 0)
INSERT [dbo].[zakaz_tovar] ([id_zakaz], [id_tovar], [Kilkist], [Znigka]) VALUES (2, 3, 3, 0)
INSERT [dbo].[zakaz_tovar] ([id_zakaz], [id_tovar], [Kilkist], [Znigka]) VALUES (1, 2, 21, 0)
INSERT [dbo].[zakaz_tovar] ([id_zakaz], [id_tovar], [Kilkist], [Znigka]) VALUES (3, 1, 4, 0)
select *from zakaz_tovar

ALTER TABLE [dbo].[tovar]  WITH CHECK ADD FOREIGN KEY([id_postav])
REFERENCES [dbo].[postachalnik] ([id_postach])
GO
ALTER TABLE [dbo].[zakaz]  WITH CHECK ADD FOREIGN KEY([id_klient])
REFERENCES [dbo].[klient] ([id_klient])
GO
ALTER TABLE [dbo].[zakaz]  WITH CHECK ADD FOREIGN KEY([id_sotrud])
REFERENCES [dbo].[sotrudnik] ([id_sotrud])
GO
ALTER TABLE [dbo].[zakaz_tovar]  WITH CHECK ADD FOREIGN KEY([id_tovar])
REFERENCES [dbo].[tovar] ([id_tovar])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[zakaz_tovar]  WITH CHECK ADD FOREIGN KEY([id_zakaz])
REFERENCES [dbo].[zakaz] ([id_zakaz])
ON UPDATE CASCADE
ON DELETE CASCADE
GO

SELECT Tovar.*
FROM Tovar;

SELECT id_postach, Nazva, Tel, Kontakt_osoba
FROM postachalnik;

SELECT City, Nazva
FROM Postachalnik
WHERE City LIKE '�%';

SELECT Nazva, price*nasklade AS Vartist
FROM Tovar;

SELECT zakaz.*
FROM zakaz
WHERE zakaz.date_naznach Between '01.01.2017' And '01.01.2018';

SELECT zakaz_tovar.id_zakaz
FROM zakaz_tovar
WHERE zakaz_tovar.id_tovar In (1,2);

SELECT zakaz_tovar.id_zakaz, Tovar.Nazva, Tovar.Price, zakaz_tovar.Kilkist
FROM Tovar INNER JOIN zakaz_tovar ON Tovar.id_tovar = zakaz_tovar.id_tovar
WHERE Tovar.Price>3 AND zakaz_tovar.Kilkist>5; 


--������ ��������� �� ����� ���� ��� ������ ������
select *
from zakaz t, zakaz_tovar g, tovar k
	where t.date_rozm <= Cast(N'2017-07-13T07:11:51.297' as datetime)and t.id_zakaz = g.id_zakaz and g.id_tovar=k.id_tovar and k.Nazva = '������';

---������ ������ ���� ���� ����������� � �������� �������� � ������� �� ����� �� ����� �� 15 �������.
select *
from tovar t
where t.NaSklade>=15 and (t.Price between 15.00 and 30.00)

---������ ��������� ��� ���� �� ��������� ���� ���������.
select * from zakaz
where date_naznach = NULL

---������ ������ ������� ������������� ������� � ���������� �������
select *
from tovar INNER JOIN postachalnik ON Tovar.id_postav=postachalnik.id_postach
where postachalnik.Nazva = '��� "����"' and tovar.NaSklade = 30

---��������� ������� ��� ���������� ������ �� ������� 30 ����, ���� ������������� � ���.
select * from postachalnik k, tovar l
where k.Nazva like N'���%' and l.id_postav = k.id_postach
select sum(tovar.Price) from tovar
INNER JOIN zakaz_tovar ON tovar.id_tovar = zakaz_tovar.id_tovar
INNER JOIN zakaz ON zakaz.id_zakaz = zakaz_tovar.id_zakaz
INNER JOIN postachalnik ON postachalnik.id_postach = tovar.id_postav
WHERE zakaz.date_naznach between '20.06.2017' And '20.07.2017' and postachalnik.Nazva like N'���%'

---��������� ���� ����������� �� ����, ���� ���� ������������� ���������� �� ������� �볺���
SELECT z.id_klient,k.id_klient, id_sotrud, date_naznach, date_rozm
FROM zakaz z, klient k
where z.id_klient=k.id_klient and k.id_klient = 3

---������ �������������, �� � ��� � �� ��������� ������.
select postachalnik.Nazva, tovar.NaSklade
from postachalnik
join tovar on postachalnik.id_postach = tovar.id_postav
where postachalnik.Nazva like N'%���%' and tovar.NaSklade = '0'

---������ �볺��� �� � �� � �������� ������ � ������������ �����.
declare @datetimeCur datetime
declare @datetimeMonAgo datetime;
Select @datetimeCur = Cast(N'2017-07-10T07:11:51.293' as DateTime)
Select @datetimeMonAgo = Cast(N'2017-06-10T07:11:51.293' as DateTime)
Select *
from zakaz z,klient k
where k.Nazva like '%��%' and z.id_klient = k.id_klient and
z.date_rozm <= @datetimeCur and z.date_rozm >= @datetimeMonAgo

---������ �����������, �� ��� ����� ������������ �� �������.
INSERT [dbo].[sotrudnik] ([id_sotrud], [Fname], [Name], [Lname], [Posada], [Adress], [City], [Home_tel]) VALUES (7, N'�������', N'�����', N'���������', N'���������', N'���. ������� 96 ��.22', N'�������', N'0639783231')
INSERT [dbo].[sotrudnik] ([id_sotrud], [Fname], [Name], [Lname], [Posada], [Adress], [City], [Home_tel]) VALUES (8, N'������', N'�����', N'�������������', N'', N'���. �������� 12 ��.78', N'�������', N'0671263531')
INSERT [dbo].[sotrudnik] ([id_sotrud], [Fname], [Name], [Lname], [Posada], [Adress], [City], [Home_tel]) VALUES (9, N'�������', N'�����', N'���������', N'���������', N'���. ����������� 3 ��.9', N'�������', N'0963823231')
Select *
From sotrudnik s
where s.Name like '%�����%'
order by s.Fname

---������ �볺��� �� ����� e-mail ������������ �� id.
Select *
From klient k
where not k.Tel is null
order by k.id_klient