use reiting
--1.�������� ������� �������� � ����� ���������.
select SUM(reiting.Reiting), dbo_student.Name_ini
from reiting join dbo_student on reiting.Kod_student = dbo_student.Kod_stud
group by dbo_student.Name_ini

--2.����������� ������� �������� � ������ ����.
select COUNT(*) as 'students', dbo_student.Kod_group
from dbo.dbo_student
group by dbo_student.Kod_group

--3.����������� ������� �������� �� ������.
select COUNT(DISTINCT Rozklad_pids.K_predm_pl) as 'subjects'
from Rozklad_pids

--4.����������� ������� ���������� ������ � ������ ����.
select COUNT(distinct Rozklad_pids.K_predm_pl) as 'subjects', 
Rozklad_pids.Kod_group from Rozklad_pids
group by Rozklad_pids.Kod_group having Rozklad_pids.Kod_group is not null

--5.����������� �������� ��� �� ������.
select AVG(reiting.reiting) as [AVG], dbo_student.Kod_group
from dbo_student join reiting on dbo_student.Kod_stud = reiting.Kod_student
group by dbo_student.Kod_group

--6.����������� �������� ��� � ���������.
select AVG(distinct reiting.Reiting) as [AVG], Rozklad_pids.K_predm_pl
from reiting join Rozklad_pids on reiting.K_zapis = Rozklad_pids.K_zapis
group by Rozklad_pids.K_predm_pl

--7.����������� �������� ������� �������� � ����� ���������.
select SUM(reiting.Reiting) as [SUM], dbo_student.Name_ini, Rozklad_pids.K_predm_pl
from (Rozklad_pids join reiting on Rozklad_pids.K_zapis = reiting.K_zapis)
join dbo_student on reiting.Kod_student = dbo_student.Kod_stud
group by  dbo_student.Name_ini, Rozklad_pids.K_predm_pl

--8.³��������� ��������� ������� �������� � ���������.
select [dbo].[predmet].Nazva,  MIN(r.Reiting) From reiting r
Inner join [dbo].[dbo_student] stud ON stud.Kod_stud = r.Kod_student
Inner join  [dbo].[Rozklad_pids] ON [dbo].[Rozklad_pids].K_zapis = r.K_zapis
Inner join [dbo].Predmet_plan ON [dbo].Predmet_plan.K_predm_pl = [dbo].[Rozklad_pids].K_predm_pl
Inner join [dbo].[predmet] ON [dbo].[predmet].K_predmet = [dbo].Predmet_plan.K_predmet
GROUP BY [dbo].[predmet].Nazva

--9.³��������� ��������� ������������ ������� � ���������.
Select [dbo].[predmet].Nazva,  MAX(r.Reiting) From reiting r
Inner join [dbo].[dbo_student] stud ON stud.Kod_stud = r.Kod_student
Inner join  [dbo].[Rozklad_pids] ON [dbo].[Rozklad_pids].K_zapis = r.K_zapis
Inner join [dbo].Predmet_plan ON [dbo].Predmet_plan.K_predm_pl = [dbo].[Rozklad_pids].K_predm_pl
Inner join [dbo].[predmet] ON [dbo].[predmet].K_predmet = [dbo].Predmet_plan.K_predmet
GROUP BY [dbo].[predmet].Nazva

--10.����������� ������� ���������� ������ �� ������ ��� ����� ���������.
select predmet.Nazva, COUNT(Rozklad_pids.K_predm_pl) from Rozklad_pids
left join Predmet_plan on Rozklad_pids.K_predm_pl = Predmet_plan.K_predm_pl
inner join predmet on predmet.K_predmet= Predmet_plan.K_predmet
group by predmet.Nazva

--11.����������� ������� ���� �� ������ �������������.
select Spetsialnost.Nazva, COUNT(*) AS kilkist
FROM (Spetsialnost JOIN Navch_plan ON Spetsialnost.K_spets = Navch_plan.K_spets)
GROUP BY Spetsialnost.Nazva

--12.����� �� �������� ����� � ������� �Reiting� �� ���������� ����� ��������(� ���� ��������� ��������� ������� ��������).
Declare @name NVARCHAR(15) = '������'
delete Reiting
FROM Reiting JOIN dbo_student ON Reiting.Kod_student = dbo_student.Kod_stud
WHERE dbo_student.Sname = @name;
select * from reiting
where Kod_student = 51

--13.����� �� �������� ����� � ������� �Para� �� ���������� ����� ���������(� ���� ��������� ��������� ����� ���������).
ALTER TABLE [dbo].[Reiting] NOCHECK CONSTRAINT [FK_Reiting_Rozklad_pids]
DECLARE @numdisc varchar(20) = '������ ������'
DELETE [dbo].[Rozklad_pids] FROM [dbo].[Rozklad_pids]
INNER JOIN [dbo].[Predmet_plan] ON [dbo].[Predmet_plan].K_predm_pl = [dbo].[Rozklad_pids].K_predm_pl
INNER JOIN [dbo].[predmet] ON [dbo].[predmet].K_predmet = [dbo].[Predmet_plan].K_predmet
WHERE [dbo].[predmet].Nazva = @numdisc
select * from Rozklad_pids
select * from Predmet_plan
select * from predmet

--14.����� �� ��������� ����� � ������� �Reiting� � ����������� ��������� ���� �� �������� ������� �� 15%.
UPDATE Reiting
SET Reiting.Reiting = Reiting.Reiting + (Reiting.Reiting * 0.15)
SELECT * FROM Reiting;

--15.����� �� ��������� ����� � ������� �Reiting�� ����������� ��������� ���� �� ����� �� 15%.
update reiting
set Reiting.Reiting = reiting.Reiting - (reiting.Reiting * 0.15)
select * from reiting

--16.����� �� ������� ����� �� ������� �Reiting� - ����������� ������� ����� �������� ��������� �����.
DECLARE @studNum int = (SELECT MIN([dbo].[dbo_student].Kod_stud) FROM [dbo].[dbo_student] WHERE [dbo].[dbo_student].Kod_group LIKE 'ϲ-54')
	WHILE @studNum <= (SELECT MAX([N_ingroup]) FROM [dbo].[dbo_student] WHERE [dbo].[dbo_student].Kod_group LIKE 'ϲ-54')
	BEGIN
		INSERT INTO Reiting 
		([K_zapis], [Kod_student], [Reiting])
		VALUES (
		(SELECT TOP 1 [dbo].[Rozklad_pids].[K_zapis] FROM [dbo].[Rozklad_pids] ORDER BY NEWID()), 
		(SELECT [dbo].[dbo_student].Kod_stud FROM [dbo].[dbo_student]
		INNER JOIN [dbo].[dbo_groups] ON [dbo].[dbo_student].Kod_group = [dbo].[dbo_groups].Kod_group
		WHERE [dbo].[dbo_groups].Kod_group LIKE 'ϲ-54' and [dbo].[dbo_student].Kod_stud LIKE @studNum
		), 
		RAND()*(100-0+100)+0
		)
		SET @studNum = @studNum + 1
	END
--17.����� �� ������� ����� �� ������� �Para� � ����������� ������� ��� ��������, ����� ���� ���������� � ����� �̻. 
ALTER TABLE [dbo].[Rozklad_pids]  alter COLUMN [Date] date  NULL
ALTER TABLE dbo.Rozklad_pids ALTER COLUMN [Kod_group] varchar(7)  NULL
ALTER TABLE dbo.Rozklad_pids ALTER COLUMN [k_vilkad] int  NULL
ALTER TABLE dbo.Rozklad_pids ALTER COLUMN [Zdacha_type] bit  NULL

INSERT INTO Rozklad_pids([K_predm_pl])
(SELECT [K_predm_pl] FROM [dbo].[Predmet_plan] 
 INNER JOIN [dbo].[predmet] ON [dbo].[predmet].K_predmet = [dbo].[Predmet_plan].K_predmet 
 WHERE [dbo].[predmet].Nazva LIKE '�%')

 select *from Rozklad_pids
--18.����� �� ��������� ����� � ����������� ���� ����������� ������ ���������� ������ �� ������ ���������� �� ���� ��������.
DECLARE @disc varchar(20) = (SELECT TOP 1 [dbo].[predmet].K_predmet FROM [dbo].[predmet] ORDER BY NEWID())
Update [dbo].[Predmet_plan]
SET [dbo].[Predmet_plan].Kilk_modul = 5
FROM [dbo].[Predmet_plan] 
INNER JOIN [dbo].[predmet] ON [dbo].[predmet].K_predmet = [dbo].[Predmet_plan].K_predm_pl
WHERE [dbo].[predmet].K_predmet LIKE @disc
select * from Predmet_plan
--19.����������� �������� �������� � ������� �Students� �� ���������� ������� �����.
ALTER TABLE [dbo].[Reiting] NOCHECK CONSTRAINT [FK_Reiting_dbo_student]

DELETE st FROM [dbo].[dbo_student] st 
INNER JOIN [dbo].[dbo_groups] ON st.Kod_group = [dbo].[dbo_groups].Kod_group
WHERE [dbo].[dbo_groups].Kod_group = 'ϲ-53'

select * from dbo_student
where Kod_group = 'ϲ-53'

--20.����� �� ������� ����� �� ������� �Reiting� � ����������� ������� ����� �������� ��������� �����.
INSERT INTO Reiting 
([K_zapis], [Kod_student], [Reiting], [Prisutn])
VALUES (
(SELECT TOP 1 [dbo].[Rozklad_pids].[K_zapis] FROM [dbo].[Rozklad_pids] ORDER BY NEWID()), 46, 85, RAND()*(1-0+1)+0)
select * from Reiting
where Reiting.Kod_student = 46

--21.����������� ��������� ����� � ������� �Reiting� ���� �Prisutnist�, ��� �������� �� ���������� �������� ����������� �������������� true.
Update [dbo].[Reiting]
SET [dbo].[Reiting].[Prisutn] = 1
FROM [dbo].[Reiting]
INNER JOIN [dbo].[dbo_student] ON [dbo].[dbo_student].Kod_stud = [dbo].[Reiting].Kod_student
WHERE [dbo].[dbo_student].Sname = '�������'
select* from Reiting
where Kod_student = 46