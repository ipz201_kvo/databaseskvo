--1.��������� ��������� ��� ��������� ���� � ������� ������, �������� ���������� ������������.
CREATE proc lab7_proc1 AS
SELECT Tovar.Nazva, tovar.Price*zakaz_tovar.Kilkist AS Vartist, sotrudnik.Fname, sotrudnik.Name 
FROM sotrudnik INNER JOIN zakaz ON sotrudnik.id_sotrud=zakaz.id_sotrud 
inner join zakaz_tovar on zakaz.id_zakaz=zakaz_tovar.id_zakaz  
INNER JOIN tovar ON tovar.id_tovar=zakaz_tovar.id_tovar
WHERE sotrudnik.Name='�����'

exec lab7_proc1 
--2.�������� ��������� ��� ��������� ���� ������ ������������� �� "����" �� 10%.
CREATE PROC lab7_proc2 AS
UPDATE tovar SET Price=Price*0.9
	WHERE id_postav=(SELECT postachalnik.id_postach 
					FROM postachalnik 
						WHERE postachalnik.Nazva='��� "����"')
exec lab7_proc2
--3.�������� ��������� ��� ��������� ���� � ������� ������, �� ������� ������� �볺��.
CREATE PROC lab7_proc3 AS
SELECT tovar.Nazva, tovar.Price*zakaz_tovar.Kilkist AS vartist, klient.Nazva FROM klient 
INNER JOIN ((tovar INNER JOIN zakaz_tovar 
ON tovar.id_tovar=zakaz_tovar.id_tovar) 
inner join zakaz 
on zakaz.id_zakaz=zakaz_tovar.id_zakaz) 
ON klient.id_klient=zakaz.id_klient 
WHERE klient.Nazva='�� ����� �.�.'
exec lab7_proc3
--4.�������� ��������� ��� ��������� ���� ������ � ������ ������� �������� �� ��������� %.
CREATE PROC lab7_proc4
@t VARCHAR(20), @p FLOAT 
AS
UPDATE tovar SET Price=Price/100 * (100-@p)
WHERE NaSklade=@t
exec lab7_proc4 @t=10, @p=5 
select *from tovar
--5.�������� ��������� ��� ��������� ���� ������ �������� ���� �������� �� ��������� %.
CREATE PROC lab7_proc5
@t VARCHAR(20)='������', @p FLOAT=0.1 AS
UPDATE tovar SET Price=Price*(1 -@p)
WHERE Nazva=@t
exec lab7_proc5  @p=0.2
--6.�������� ��������� ��� ���������� �������� ������� ������, �������� �� ���������� �����.
CREATE PROC lab7_proc6 
@m INT, @s FLOAT OUTPUT 
AS
SELECT @s=Sum(tovar.Price*zakaz_tovar.Kilkist)
	FROM tovar INNER JOIN zakaz_tovar ON tovar.id_tovar=zakaz_tovar.id_tovar
				inner join zakaz on zakaz.id_zakaz=zakaz_tovar.id_zakaz
	GROUP BY Month(zakaz.date_naznach)
	HAVING Month(zakaz.date_naznach)=@m
declare @s FLOAT
exec lab7_proc6 6, @s OUTPUT
print '�������� ������� ������:' 
print @s
--7.�������� ��������� ��� ���������� �������� ������� ������, ��������� ������� � ���, �� ����������� ��������� ����������.
CREATE PROC lab7_subproc7
@f VARCHAR(20) OUTPUT, @n VARCHAR(20)
 AS
SELECT @f=City FROM klient WHERE Nazva=@n

declare @f varchar(20)
exec lab7_subproc7 @f output, @n = '��� "����"'
Print @f

Create proc lab7_proc7
@suma float output, @n varchar(20)
as
declare @pr varchar(20)
exec lab7_subproc7 @pr output, @n
select @suma=(tovar.Price*zakaz_tovar.kilkist)
from zakaz inner join zakaz_tovar on zakaz.id_zakaz = zakaz_tovar.id_zakaz
inner join tovar on zakaz_tovar.id_tovar = tovar.id_tovar
where zakaz.id_klient IN (select id_klient from klient
where city = @pr)

declare @f varchar(20)
exec lab7_proc7 @f output, @n = '��� "����"'
Print @f