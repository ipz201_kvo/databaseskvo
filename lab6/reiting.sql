CREATE PROC AverageReitingWithoutRetakes AS
SELECT S.Kod_stud, S.Name_ini, AVG(R.Reiting) as AverageReitingWithoutRetakes FROM Reiting R
JOIN dbo_student S ON S.Kod_stud = R.Kod_student
JOIN (SELECT distinct max(RP.K_zapis) over (partition by S.Name_ini, P.K_predmet) as new_K_zapis
		FROM Rozklad_pids RP
		JOIN dbo_groups G ON G.Kod_group = RP.Kod_group
		JOIN dbo_student S ON S.Kod_group = G.Kod_group
		JOIN Predmet_plan PP ON PP.K_predm_pl = RP.K_predm_pl
		JOIN predmet P ON P.K_predmet = PP.K_predmet) DR ON DR.new_K_zapis = R.K_zapis
JOIN Rozklad_pids RP ON RP.K_zapis = DR.new_K_zapis
JOIN Predmet_plan PP ON PP.K_predm_pl = RP.K_predm_pl
JOIN predmet P ON P.K_predmet = PP.K_predmet
GROUP BY S.Name_ini, S.Kod_stud
ORDER BY S.Name_ini

exec AverageReitingWithoutRetakes

CREATE PROC AverageReitingWithRetakes AS
SELECT S.Name_ini, AVG(R.Reiting) as AverageReitingWithRetakes FROM dbo_student S
JOIN Reiting R ON S.Kod_stud = R.Kod_student
JOIN Rozklad_pids RP ON R.K_zapis = RP.K_zapis
JOIN Predmet_plan PP ON RP.K_predm_pl = PP.K_predm_pl
JOIN predmet P ON PP.K_predmet = P.K_predmet
GROUP BY S.Name_ini
ORDER BY S.Name_ini

exec AverageReitingWithRetakes
drop proc StudentsReiting
CREATE PROC StudentsReiting AS
SELECT * FROM (SELECT R.Name_ini, 
	CASE 
		WHEN R.AverageReitingWithRetakes > 70 THEN '3+'
		WHEN R.AverageReitingWithRetakes > 60 THEN '3'
		WHEN R.AverageReitingWithRetakes < 60 THEN '2'
	END Reiting
	FROM (SELECT S.Name_ini, AVG(R.Reiting) as AverageReitingWithRetakes FROM dbo_student S
		JOIN Reiting R ON S.Kod_stud = R.Kod_student
		JOIN Rozklad_pids RP ON R.K_zapis = RP.K_zapis
		JOIN Predmet_plan PP ON RP.K_predm_pl = PP.K_predm_pl
		JOIN predmet P ON PP.K_predmet = P.K_predmet
		GROUP BY S.Name_ini) AS R) AS RR
WHERE RR.Reiting is not null

exec StudentsReiting

drop proc StudentsNationalAndECTSReiting
CREATE PROC StudentsNationalAndECTSReiting AS
SELECT R.Name_ini, 
	CASE 
		WHEN R.AverageReitingWithRetakes > 90 THEN '5'
		WHEN R.AverageReitingWithRetakes > 75 THEN '4'
		WHEN R.AverageReitingWithRetakes > 50 THEN '3'
		WHEN R.AverageReitingWithRetakes > 0 THEN '2'
	END NationalReiting,
	CASE
		WHEN R.AverageReitingWithRetakes > 90 THEN 'A'
		WHEN R.AverageReitingWithRetakes > 80 THEN 'B'
		WHEN R.AverageReitingWithRetakes > 65 THEN 'C'
		WHEN R.AverageReitingWithRetakes > 55 THEN 'D'
		WHEN R.AverageReitingWithRetakes > 50 THEN 'E'
		WHEN R.AverageReitingWithRetakes > 35 THEN 'FX'
		WHEN R.AverageReitingWithRetakes > 0 THEN 'F'
	END ECTS
	FROM (SELECT S.Name_ini, AVG(R.Reiting) as AverageReitingWithRetakes FROM dbo_student S
		JOIN Reiting R ON S.Kod_stud = R.Kod_student
		JOIN Rozklad_pids RP ON R.K_zapis = RP.K_zapis
		JOIN Predmet_plan PP ON RP.K_predm_pl = PP.K_predm_pl
		JOIN predmet P ON PP.K_predmet = P.K_predmet
		GROUP BY S.Name_ini) AS R

exec StudentsNationalAndECTSReiting